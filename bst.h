/* binary search tree (BST) header file */

#ifndef _bst_h_
#define _bst_h_

struct bst {
	void *data;
	struct bst *parent;
	struct bst *left;
	struct bst *right;
	struct bst *next;
	unsigned size;
};

struct bst *bst_create(void);
void bst_destroy(struct bst **const t, int (*compar)(const void *,
	const void *));
/* same as 'bst_destroy' but it doesn't call free() on released data */
void bst_destroy_without_free(struct bst **const t, int (*compar)(const void *,
	const void *));

/* inserts given data into given tree using comparing function 'compar'. returns
 * poiter to inserted subtree (leaf) or NULL (= data already contained) */
struct bst *bst_insert(struct bst **const t, void *data,
	int (*compar)(const void *, const void *));
/* removes given data from given tree using comparing function 'compar'. returns
 * pointer to removed data (NULL if not found) */
void *bst_remove(struct bst **const t, const void *data, int (*compar)(const
			void *, const void *));

/* finds given data in given tree using comparing function 'compar'. returns 1
 * if found, 0 otherwise */
int bst_contains(const struct bst *t, const void *data,
	int (*compar)(const void *, const void *));
/* returns 1 if given tree doesn't have any nodes (no data) */
int bst_empty(const struct bst *const t);
unsigned bst_size(const struct bst *const t);

/* returns leftmost subtree */
struct bst *bst_leftmost(struct bst *t);
/* returns rightmost subtree */
struct bst *bst_rightmost(struct bst *t);
/* returns first (leftmost) */
struct bst *bst_first(struct bst *t);
/* returns last (rightmost) */
struct bst *bst_last(struct bst *t);

#endif /* _bst_h_ */
