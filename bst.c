/* implementation of binary search tree (BST) */

#include <assert.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "./bst.h"

static int bst_is_leaf(const struct bst *const t) {
  return t->left == NULL && t->right == NULL;
}

static struct bst *bst_init(void *data) {
  struct bst *t = NULL;
  t = (struct bst *) malloc(sizeof(struct bst));
  if (t == NULL) {
    perror("malloc");
    exit(1);
  }
  t->data = data;
  t->size = 1;
  t->parent = NULL;
  t->left = NULL;
  t->right = NULL;
  return t;
}

struct bst *bst_create(void) {
  return (struct bst *) NULL;
}

void bst_destroy(struct bst **const t, int (*compar)(const void *, const void
      *)) {
  void *data = NULL;
  assert(t != NULL);
  while (!bst_empty(*t)) {
    data = bst_rightmost(*t);
    bst_remove(t, data, compar);
    if (data != NULL) free(data);
  }
}

void bst_destroy_without_free(struct bst **const t, int (*compar)(const void *,
      const void *)) {
  void *data = NULL;
  assert(t != NULL);
  while (!bst_empty(*t)) {
    data = bst_rightmost(*t);
    bst_remove(t, data, compar);
  }
}

int bst_contains(const struct bst *t, const void *data, int (*compar)(const void
      *, const void *)) {
  int r;
  while (t != NULL) {
    r = compar(t->data, data);
    if (r == 0) return 1;
    /* TODO FIXME XXX */
    else if (r > 0) t = t->left;
    else t = t->right;
  }
  return 0;
}

int bst_empty(const struct bst *const t) {
  return t == NULL;
}

unsigned bst_size(const struct bst *const t) {
  return bst_empty(t) ? 0 : t->size;
}

struct bst *bst_leftmost(struct bst *t) {
  assert(t != NULL);
  while (t->left != NULL)
    t = t->left;
  return t;
}

struct bst *bst_rightmost(struct bst *t) {
  assert(t != NULL);
  while (t->right != NULL)
    t = t->right;
  return t;
}

struct bst *bst_first(struct bst *t) {
  return bst_leftmost(t);
}

struct bst *bst_last(struct bst *t) {
  return bst_rightmost(t);
}

/***************************************************************/


struct bst *bst_insert(struct bst **const t, void *data, int (*compar)(const
      void *, const void *)) {
  struct bst *r, *s;
  int comp;
  assert(t != NULL);
  if (*t == NULL) {
    *t = bst_init(data);
    return *t;
  }
  r = *t;
  while (1) {
    comp = compar(r->data, data);
    if (comp == 0) {
      return NULL;;
    } else if (comp > 0) {
      if (r->left == NULL) {
        s = bst_init(data);
        r->left = s;
        s->parent = r;
        do { r->size++; } while ((r = r->parent) != NULL);
        return s;
      } else {
        r = r->left;
      }
    } else if (comp < 0) {
      if (r->right == NULL) {
        s = bst_init(data);
        r->right = s;
        s->parent = r;
        do { r->size++; } while ((r = r->parent) != NULL);
        return s;
      } else {
        r = r->right;
      }
    }
  }
  return NULL;
}

void *bst_remove(struct bst **const t, const void *data, int (*compar)(const
      void *, const void *)) {
  struct bst *r = NULL, *s = NULL;
  void *removed = NULL;
  int comp;
  assert(t != NULL);
  r = *t;
  while (r != NULL && (comp = compar(r->data, data)) != 0) {
    if (comp > 0) r = r->left;
    else r = r->right;
  }
  if (r == NULL) return NULL;
  if (bst_is_leaf(r)) {
    if (r->parent == NULL) {
      removed = r->data;
      free(r);
      *t = NULL;
    } else {
      if (compar(r->parent->data, r->data) > 0) r->parent->left = NULL;
      else r->parent->right = NULL;
      s = r->parent;
      do { s->size--; } while ((s = s->parent) != NULL);
      removed = r->data;
      free(r);
    }
  } else if (r->left == NULL /* && r->right != NULL */) {
    if (r->parent == NULL) {
      r->right->parent = NULL;
      *t = r->right;
      removed = r->data;
      free(r);
    } else {
      if (compar(r->parent->data, r->data) > 0) r->parent->left = r->right;
      else r->parent->right = r->right;
      r->right->parent = r->parent;
      s = r->parent;
      do { s->size--; } while ((s = s->parent) != NULL);
      removed = r->data;
      free(r);
    }
  } else if (r->right == NULL /* && r->left != NULL */) {
    if (r->parent == NULL) {
      r->left->parent = NULL;
      *t = r->left;
      free(r);
    } else {
      if (compar(r->parent->data, r->data) > 0) r->parent->left = r->left;
      else r->parent->right = r->left;
      r->left->parent = r->parent;
      s = r->parent;
      do { s->size--; } while ((s = s->parent) != NULL);
      removed = r->data;
      free(r);
    }
  } else {
    void *left_max = NULL;
    struct bst *left_subtree = r->left;
    left_max = (bst_rightmost(left_subtree))->data;
    if (compar(r->left->data, left_max) == 0) {
      if (r->left->left == NULL) {
        removed = r->left->data;
        free(r->left);
        r->left = NULL;
      } else {
        struct bst *tmp = r->left;
        r->left = r->left->left;
        removed = tmp->data;
        free(tmp);
        r->left->parent = r;
      }
      r->size--;
    } else {
      bst_remove(&left_subtree, left_max, compar);
      /* r->size was already decreased in bst_remove() */
    }
    r->data = left_max;
  }
  return removed;
}
