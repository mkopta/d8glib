#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "./bst.h"
#include "./list.h"

void print_list(struct list *l) {
	struct list_node *n = l->head;
	while (n != NULL) {
		printf("%d ", *((int *) n->data));
		n = n->next;
	}
	putchar('\n');
}

void test_list(void) {
	struct list *l = list_create();
	struct list *l2 = list_create();
	int *array = (int *) malloc(sizeof(int) * 100);
	int i;
	assert(array != NULL);
	for (i = 0; i < 100; i++)
		array[i] = i*2;
	printf("Size: %u\n", list_size(l));
	list_push(l, array);
	list_push(l, array+1);
	list_push(l, array+2);
	list_unshift(l, array+3);
	list_unshift(l, array+4);
	list_unshift(l, array+5);
	for (i = 0; i < 100; i++)
		list_push(l2, array+i);
	printf("Size l1: %u\n", list_size(l));
	print_list(l);
	printf("Size l2: %u\n", list_size(l2));
	print_list(l2);
	list_append_list(l, l2);
	printf("Size l1: %u\n", list_size(l));
	print_list(l);
	(void) list_pop(l);
	(void) list_shift(l);
	print_list(l);
	printf("Size: %u\n", list_size(l));
	list_purge(l);
	printf("Size: %u\n", list_size(l));
	list_destroy(l);
	list_destroy(l2);
	free(array);
}

int int_compare(const void *a, const void *b) {
	if (*((const int *) a) >  *((const int *) b)) return  1;
	if (*((const int *) a) <  *((const int *) b)) return -1;
	/*if (*((const int *) a) == *((const int *) b)) return  0;*/
	return 0;
}

void test_bst(void) {
	struct bst *t = bst_create();
	/*
	   bst_insert(&t, 10);
	   bst_insert(&t, 5);
	   bst_insert(&t, 15);
	   printf("Size: %d\n", bst_size(t));
	   puts("----------------");
	   bst_remove(&t, 10);
	   printf("Size: %d\n", bst_size(t));
	   bst_insert(&t, 15);
	   printf("Size: %d\n", bst_size(t));
	   bst_destroy(&t);
	   printf("Size: %d\n", bst_size(t));
	   bst_insert(&t, 10);
	   bst_insert(&t, 5);
	   bst_insert(&t, 15);
	   bst_insert(&t, 20);
	   bst_insert(&t, 13);
	   bst_insert(&t, 14);
	   bst_insert(&t, 5);
	   bst_insert(&t, 4);
	   bst_insert(&t, 2);
	   bst_insert(&t, 7);
	   bst_insert(&t, 6);
	   bst_insert(&t, 9);
	   bst_insert(&t, 8);
	   bst_insert(&t, 1);
	   bst_insert(&t, 3);
	   bst_insert(&t, 11);
	   bst_insert(&t, 12);
	   bst_insert(&t, 16);
	   bst_insert(&t, 17);
	   bst_insert(&t, 18);
	   bst_insert(&t, 19);
	   bst_insert(&t, 21);
	 */
	bst_destroy(&t, int_compare);
	puts("----------------");
	printf("Size: %d\n", bst_size(t));
}

void test_bst2(void) {
	/*
	   struct bst *t = bst_create();
	   int *a = NULL;
	   unsigned size;
	   bst_insert(&t, 10);
	   bst_insert(&t, 5);
	   bst_insert(&t, 15);
	   bst_insert(&t, 20);
	   bst_insert(&t, 13);
	   bst_insert(&t, 14);
	   bst_insert(&t, 5);
	   bst_insert(&t, 4);
	   bst_insert(&t, 7);
	   bst_print(t);
	   a = bst_to_array(t);
	   size = bst_size(t);
	   bst_destroy(&t);
	   t = NULL;
	   print_array(a, size);
	   t = bst_from_array(a, size);
	   bst_print(t);
	   if (a) free(a);
	   bst_destroy(&t);
	 */
}

int main(void) {
	test_bst();
	return 0;
}
