/* bidirectional linked list header file */

#ifndef _list_h_
#define _list_h_

struct list_node {
	void *data;
	struct list_node *prev;
	struct list_node *next;
};

struct list {
	struct list_node *head;
	struct list_node *tail;
	unsigned size;
};

/* you can use 'struct list' manualy or you can use:
 *   list_create for creating a new list
 *   list_delete for deleting such a list
 * DO NOT call list_delete on manualy created list!
 */
struct list *list_create(void);
void list_destroy(struct list *const l);
/* just removes all elements */
void list_purge(struct list *const l);
void list_purge_without_free(struct list *const l);

void *list_pop(struct list *const l);
void list_push(struct list *const l, void *data);
void *list_shift(struct list *const l);
void list_unshift(struct list *const l, void *data);

/* places given data at given position into given list and returns old data at
 * that position (because it is a pointer to something which should maybe be
 * free()d */
void *list_insert_at(struct list *const l, unsigned position, void *data);
void *list_get_at(const struct list *const l, const unsigned position);

/* inserts all elements from 'source' into 'dest' which are not contained in
 * 'dest' already. This condition is checked using 'compar' function */
unsigned list_insert_list_uniq(const struct list *source, struct list *dest, int
		(*compar)(const void *, const void *));
/* append list 'b' at the end of list 'a'.
 * Returns number of appended elements */
unsigned list_append_list(struct list *a, struct list *b);

unsigned list_size(const struct list *const l);
int list_empty(const struct list *const l);

int list_contains(const struct list *const l, const void *data,
		int (*compar)(const void *, const void *));
struct list_node *list_find(const struct list *l, const void *data,
		int (*compar)(const void *, const void *));
struct list_node *list_find_first_duplicate(const struct list *l,
		int (*compar)(const void *, const void *));
struct list_node *list_find_first_common(const struct list *a,
		const struct list *b,
		int (*compar)(const void *, const void *));

#endif /* _list_h_ */
