CC        = cc
CFLAGS    = -Wextra -Wconversion -Wwrite-strings
CFLAGS   += -W -Wall -ansi -pedantic -Wcast-align
CFLAGS   += -Wcast-qual -Wchar-subscripts -Winline
CFLAGS   += -Wpointer-arith -Wredundant-decls -Wshadow
OBJS      = bst.o list.o
HEADERS   = bst.h list.h

all: clean test run

test: ${HEADERS} ${OBJS}
	$(CC) $(CFLAGS) -o test test.c ${OBJS}

list.o: list.c list.h
	$(CC) $(CFLAGS) -c -o list.o list.c

bst.o: bst.c bst.h
	$(CC) $(CFLAGS) -c -o bst.o bst.c

run:
	./test

clean:
	rm -f *.o test
